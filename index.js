import { AppRegistry } from 'react-native';
import Root from './src/routes';

AppRegistry.registerComponent('ZooMemoryGame', () => Root);
