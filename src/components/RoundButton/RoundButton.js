import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native'
import { Spinner } from '..'
import Styles from './style'


const RoundButton = props => {
    const style = props.style instanceof Array ? [Styles.roundButton, props.disabled ? Styles.disabled : {}].concat(props.style) : [Styles.roundButton, props.style, props.disabled ? Styles.disabled : {}]
    const textStyle = props.textStyle instanceof Array ? [Styles.text].concat(props.textStyle) : [Styles.text, props.textStyle]
    const { disabled, onPress, textLevel, numberLevel, viewButtonstyle } = props
    const { outerTopButton, roundButton, textLevelStyle, viewNumberLevel, numberLevelStyle } = Styles
    return (
        <TouchableOpacity
            disabled={disabled}
            onPress={() => onPress()} >
            <View style={outerTopButton}>

                <View style={[roundButton, viewButtonstyle, disabled ? Styles.disabled : {}]}>
                    <View>
                    </View>
                    <View>
                        <Text style={[textLevelStyle, textStyle]}>
                            {props.textLevel}
                        </Text>
                    </View>
                    <View style={viewNumberLevel}>
                       <Text style={numberLevelStyle}>{numberLevel}</Text>
                    </View>

                    {props.loading ?
                        <Spinner
                            color='blue'
                            style={{ position: 'absolute', right: '20%' }}
                            {...props.spinnerProps} />
                        : null}
                </View>
            </View>

        </TouchableOpacity>
    )
}


RoundButton.defaultProps = {
    onPress: () => { }
}
export default RoundButton