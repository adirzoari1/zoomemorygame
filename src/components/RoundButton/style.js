import { StyleSheet, Platform } from 'react-native'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts'

import { calcSize } from '../../utils/Consts';

const Styles = StyleSheet.create({
    outerTopButton: {
        backgroundColor: colors.yellow_a,
        width: calcSize(464),
        height: calcSize(109),
        borderRadius: calcSize(70),
        borderColor: colors.yellow_a,
        alignItems: 'center',
        justifyContent: 'center',
        margin:calcSize(20)


    },
    roundButton: {
        backgroundColor: colors.yellow,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        width: calcSize(454),
        height: calcSize(99),
        borderRadius: calcSize(70),


    },
    textLevelStyle: {
        color: colors.white,
        fontSize: calcSize(63),
        fontFamily: Fonts.mon_black
    },
    disabled: {
        opacity: 0.6
    },
    viewNumberLevel: {
        width: calcSize(77),
        height: calcSize(77),
        borderRadius: calcSize(39),
        backgroundColor: colors.orange_a,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: calcSize(10)

    },
    numberLevelStyle: {
        color: colors.white,
        fontSize: calcSize(46),
        fontFamily: Fonts.mon_black
    }
})

export default Styles
