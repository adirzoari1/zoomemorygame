import { StyleSheet,Platform } from 'react-native'
import { calcSize } from '../../utils/Consts';
import colors from '../../utils/Colors'

const Styles = StyleSheet.create({
    
    container:{
        flex:1,
        position: 'absolute',
        right: 0,
        left: 0,
        top: 0,
        bottom: 0,
        justifyContent:'center',
        alignItems:'center',
    },
    viewSpinner:{
        backgroundColor:colors.light_white,
        width:calcSize(50),
        height:calcSize(50),
        borderRadius:calcSize(15),
       
        justifyContent:'center',
        alignItems:'center'
    },
    iconSpinner:{   
        backgroundColor:colors.transparent,
        fontSize:calcSize(25),
        color:colors.light_black
    }
})

export default Styles
