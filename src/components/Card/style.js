import { StyleSheet } from 'react-native'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';
import { calcSize } from '../../utils/Consts'

const Styles = StyleSheet.create({
    card: {
        flex:1,
    },
    animatedContainer:{

        margin:calcSize(20)

    },
    cardContainer: {
        backgroundColor: colors.white,
        width: calcSize(306),
        height: calcSize(306),
    },
    face: {
        flex: 1,
        width: calcSize(306),
        height: calcSize(306),

    },
    back: {
        flex: 1,

    },
    imgFace: {
        width: calcSize(294),
        height: calcSize(294),
    },
    imgBack:{
        backgroundColor: colors.white,
        width: calcSize(294),
        height: calcSize(294),
        justifyContent:'center',
        borderRadius:calcSize(25)

    },
    imgCardBack:{
        width:calcSize(278),
        height: calcSize(272),
        borderRadius:calcSize(25)

    }
})

export default Styles
