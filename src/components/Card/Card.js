import React, { Component } from 'react'
import {
    Animated,
    Easing,
    TouchableOpacity,
    Text,
    View,
    ImageBackground,
    Image
} from 'react-native'

import Styles from './style'

import { calcSize } from '../../utils/Consts';
import colors from '../../utils/Colors';

const imagesArray = [
    {source:require('../../assets/images/animals/img_animal1.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal2.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal3.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal4.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal5.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal6.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal7.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal9.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal10.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal11.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/animals/img_animal12.png'),sound:'../../assets/sounds'},


    
    
]
const elroyYamelArrayImages = [
    {source:require('../../assets/images/family1/img_elyam1.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam2.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam3.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam4.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam5.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam6.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam7.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam8.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam9.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family1/img_elyam10.png'),sound:'../../assets/sounds'},
]

const nitayRomiArrayImages = [
    {source:require('../../assets/images/family2/img_romiNita1.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita2.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita3.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita4.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita5.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita6.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita7.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita8.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita9.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita10.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita11.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita12.png'),sound:'../../assets/sounds'},
    {source:require('../../assets/images/family2/img_romiNita13.png'),sound:'../../assets/sounds'},

]
export default class Card extends Component {
    constructor(props) {
        super(props);
   
        this.state = {
            animatedValue: new Animated.Value(0),
            isFlipped: true,
            image:'',
            isMatched: false,
            isOpened: false,
            key:''
        };

        this._flipToggleCard = this._flipToggleCard.bind(this);
        this._flippedCard = this._flippedCard.bind(this);
        this.flippedCardView = this.flippedCardView.bind(this);
        this._renderBack = this._renderBack.bind(this)
        this._renderFront = this._renderFront.bind(this)
        this._setFlipped = this._setFlipped.bind(this)
        this._setMatch = this._setMatch.bind(this)
    };

    componentDidMount(){
        const { isMatched, isOpened, key, value } = this.props.data
        this.setState({image: value, isMatched,isOpened,key})
    }

    componentDidUpdate(prevProp, prevState) {
        if (this.state.isFlipped !== prevState.isFlipped) {
            this._flippedCard();
        }
    };

    componentWillReceiveProps(nextProps){
        this.setState({disabledCard:nextProps.disabledCard})
    }

    _flipToggleCard() {
        this.setState({ isFlipped: !this.state.isFlipped, isOpened:true });
        this.props.onPress(this)
    };

    _flippedCard() {
        Animated.spring(this.state.animatedValue, {
            toValue: 0,   // Returns to the start
            // tension: this.props.tension, // Slow
            friction: this.props.friction,  // Oscillate a lot
            useNativeDriver:true

        }).start();
    };

    _setFlipped(){
        this.setState({isFlipped:!this.state.isFlipped,isOpened:false})
    }
    _setMatch(){

        this.setState({isOpened:true,backgroundTransparent:true})
    }
    _renderFront() {
        const { imgFace } = Styles
        const { divideStyle} = this.props
        const imgFaceObject = {width: calcSize(294/divideStyle), height: calcSize(294/divideStyle)}
        return (

            <ImageBackground resizeMode='contain' style={[imgFace,imgFaceObject]} source={require('../../assets/images/img_cart1.png')} >
            </ImageBackground>
        )
    }
    //Desired screen view method in back page
    _renderBack() {
        const { imgBack, imgCardBack} = Styles
        const { backgroundTransparent, image} = this.state
        const { divideStyle } = this.props
        const imgBackObject = {width: calcSize(294/divideStyle), height: calcSize(294/divideStyle), borderRadius: calcSize(25/divideStyle)}
        // const imgCardBackObject = {width: calcSize(278/divideStyle), height: calcSize(272/divideStyle)}
        const imgCardBackObject = {width: calcSize(294/divideStyle), height: calcSize(294/divideStyle), borderRadius: calcSize(25/divideStyle)}

        const backgroundTransparentObject = backgroundTransparent? {opacity: 0} :{}
        const a =nitayRomiArrayImages // TODO: switch case with any catergory: animals,elroyamel,nitayRomi ...

        return (
            // <View style={[imgBack,imgBackObject,backgroundTransparentObject]}>
          <Image source={elroyYamelArrayImages[image].source} style={[imgCardBack,imgCardBackObject,backgroundTransparentObject]} resizeMode='contain' resizeMethod='resize' />
            // </View>
        )
    }



    flippedCardView(isFlipped) {
        if (isFlipped) {
            return this._renderFront();
        } else {
            return this._renderBack();
        }
    }
    render() {
        const { isOpened, backgroundTransparent } = this.state
        const { disabledCard, divideStyle, marginStyle } = this.props
        const rotateX = this.state.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '180deg']
        });
        
        const onPress = isOpened? ()=>{} : this._flipToggleCard 
        const disabled  = disabledCard || isOpened?  true: false
        const animatedContainer = {margin: calcSize(20/marginStyle)}
        return (
            <TouchableOpacity disabled = {disabled} onPress={onPress} style={animatedContainer}>
                <Animated.View
                    style={[animatedContainer, { transform: [{ rotateX }] }]}>
                    {this.flippedCardView(this.state.isFlipped)}
                </Animated.View>
            </TouchableOpacity>);
    };

}


