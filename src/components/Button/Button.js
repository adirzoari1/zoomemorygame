import React from 'react'
import {
    Platform,
    View,
    TouchableOpacity,
    TouchableNativeFeedback
} from 'react-native'

const _handlePress = callback => requestAnimationFrame(callback)


const Button = (props) => {
    return (
        <TouchableOpacity disabled={props.disabled} style={props.style} onPress={props.onPress}>{props.children}</TouchableOpacity> 
    )
}

Button.defaultProps = {
    onPress : () => {}
}

export default Button