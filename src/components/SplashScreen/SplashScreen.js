import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    ImageBackground
} from 'react-native'

import Styles from './style'


export default class SplashScreen extends Component {

    render() {
        const { container, textSlogan, textLogo } = Styles
        return (
            <View style={container}>
            <SplashScreen/>
            </View>
        )
    }
}

