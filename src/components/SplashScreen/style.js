import { StyleSheet } from 'react-native'
import colors from '../../utils/Colors'
import Fonts from '../../utils/Fonts';
import {calcSize} from '../../utils/Consts'

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        position: 'relative',
        minWidth:'100%',
        minHeight:'100%',
        justifyContent:'center',
        alignItems:'center'
    },

})

export default Styles
