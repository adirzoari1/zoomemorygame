import React from 'react';
import { View, Text, ImageBackground, YellowBox } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import { HomeScreen, GameScreen } from '../screens'

const RootStack = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  Game: {
    screen: GameScreen
  }
});

export default class App extends React.Component {
  constructor(props) {
    super(props)
    YellowBox.ignoreWarnings(

      ['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'

      ]);
  }
  render() {
    return (

      <RootStack />
    );
  }
}