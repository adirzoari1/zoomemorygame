import { StyleSheet, Dimensions, PixelRatio } from 'react-native'
import { calcSize } from '../../utils/Consts'
import colors from '../../utils/Colors'
import { DIMENSIONS } from '../../utils/Consts';
import Fonts from '../../utils/Fonts';


const Styles = StyleSheet.create({
    container: {
        flex: 1,
     //   alignItems: 'center',
       // justifyContent:'center'
    },
    viewCards:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems:'flex-start',
    },
    viewHeader:{
        backgroundColor:colors.grey,
        width: Dimensions.width - calcSize(40),
        height: calcSize(80),
        borderRadius: calcSize(20),
        margin: calcSize(30),
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    textHeader:{
        color: colors.blue,
        fontSize:calcSize(40),
        fontFamily: Fonts.mon_black
    }


})

export default Styles
