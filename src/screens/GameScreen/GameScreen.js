import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Image
} from 'react-native'

import { Button, Card } from '../../components'
import Styles from './style'
import _ from 'lodash'
import { calcSize } from '../../utils/Consts';
import { playSound } from '../../utils/Utils';

class GameScreen extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props)
        this.state = {
            numberLevel: '',
            arrayImages: [],
            cardsPressed: 0,
            cardOne: {},
            cardTwo: {},
            matches: 0,
            disabledCard: false,
            divideStyle:'',
            marginStyle:'',
            level:1,
            score: 20

        }
        this.buildGame = this.buildGame.bind(this)
        this.buildArrayImages = this.buildArrayImages.bind(this)
        this.flippedCard = this.flippedCard.bind(this)
        this.comparedCards = this.comparedCards.bind(this)
        this.checkGameEnd = this.checkGameEnd.bind(this)
        this.resetCardsNotEqual  = this.resetCardsNotEqual.bind(this)
        this.generateRandomNumberRange = this.generateRandomNumberRange.bind(this)
    }


    componentDidMount() {
        const numberLevel = this.props.navigation.state.params.numberLevel
        this.setState({ numberLevel });
        this.buildGame(numberLevel)
    }
    buildGame = (numberLevel) => {
        switch (numberLevel) {
            case '1': {
                this.setState({ numberCards: 3,divideStyle:1,marginStyle:1, level: 1})
                let valuesArray = this.buildArrayImages(3)
                break;
            }
            case '2': {
                this.setState({ numberCards: 6, divideStyle:1.5,marginStyle:1.5, level:2 })
                let valuesArray = this.buildArrayImages(6)
                break;
            }
            case '3': {
                this.setState({ numberCards: 10, divideStyle:2, marginStyle:2, level:3 })
                let valuesArray = this.buildArrayImages(10)

                break;
            }
        }
    }

    buildArrayImages = (length) => {
        let values = []
        for (var i = 0; i < length; i++) {
            let image = i;
            values.push(image)
        }
        console.log(values)
        // values = this.generateRandomNumberRange(length)
        let tempValues = values.map(i => { return [i, i] })
            .reduce((a, b) => { return a.concat(b) })
        let suffleValues = _.shuffle(tempValues)
        let arrayImages = suffleValues.map((value, key) => {
            return {
                isOpened: false,
                isMatched: false,
                key,
                value
            }
        })
        this.setState({ arrayImages })
    }

    generateRandomNumberRange(length){
        let values = []
        while(values.length<length){
            var r = _.random(0, 10);
            if(!_.includes(values,r)){
                values.push(r)
            }
        }

        return values;

    }
    flippedCard(card) {
        this.setState({ disabledCard: true })

        const { cardsPressed } = this.state
        // compare between cards
        if (cardsPressed == 1) {
            this.setState({ cardTwo: card, cardsPressed: 2 }, () => {
                this.comparedCards()
            })

        } else {
            this.setState({ cardsPressed: 1, cardOne: card })
            this.setState({ disabledCard: false })


        }

    }

    comparedCards() {
        const { cardOne, cardTwo, matches } = this.state
        if (cardOne.state.image == cardTwo.state.image) {
            this.setState({ matches: matches + 1 }, () => {
                this.checkGameEnd()
                setTimeout(()=>{
                    cardOne._setMatch()
                    cardTwo._setMatch()
                    this.setState({ disabledCard: false })

                },1300)
               

            })


        } else {
            playSound('sound_unmatch.mp3')

            setTimeout(() => {
                cardOne._setFlipped()
                cardTwo._setFlipped()
                this.resetCardsNotEqual()
                this.setState({ disabledCard: false })
            }, 1000)

        }

    }
    checkGameEnd() {
        const { matches, numberCards } = this.state
        if (matches == numberCards) {
            playSound('sound_win.mp3')

            alert('finish game')

        }else{
            playSound('sound_match.mp3')

        }
    }
    resetCardsNotEqual() {

        this.setState({ cardsPressed: 0, cardOne: {}, cardTwo: {} })
    }


    render() {
        const { container, viewCards, viewHeader, textHeader} = Styles
        const { disabledCard, divideStyle, marginStyle, level, score } = this.state
        let cards = this.state.arrayImages && this.state.arrayImages.map((value, key) => (
            <Card
                onPress={this.flippedCard}
                velocity={1000} // Velocity makes it move
                tension={3} // Slow
                friction={6} // Oscillate a lot
                data={value}
                key={key}
                disabledCard={disabledCard}
                divideStyle = {divideStyle}
                marginStyle = {marginStyle}
            />

        ))
        return (
            <ImageBackground resizeMode='cover' style={container} source={require('../../assets/images/img_background.jpg')} >
                <View style={viewHeader}>
                    <Text style={textHeader}>Level {level}</Text>
                    {/* <Text></Text>
                    <Text style={textHeader}>Score {score}</Text> */}
                </View>
                <View style={viewCards}>
                    {cards}
                </View>

            </ImageBackground>


        )
    }
}

export default GameScreen