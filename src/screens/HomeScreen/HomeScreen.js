import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Image
} from 'react-native'

import { Button, RoundButton } from '../../components'
import Styles from './style'



class HomeScreen extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props)
    }


    navigateToGame = (numberLevel) => {
        this.props.navigation.navigate('Game', { numberLevel })
    }


    render() {
        const { container, viewLogo, imgLogo, textLogo, imgBtn, viewSettings,  imgSet } = Styles
        return (
            <ImageBackground resizeMode='cover' style={container} source={require('../../assets/images/img_background.jpg')} >
                <View style={viewLogo}>
                    <Image source={require('../../assets/images/img_rainbow.png')} style={imgLogo} resizeMethod='resize' />
                    <Text style={textLogo}>Zoo Memory Game</Text>
                </View>

                <View>
                    <Button onPress={() => { this.navigateToGame('1') }}>
                        <Image source={require('../../assets/images/img_btn1.png')} style={imgBtn} resizeMethod='resize' />
                    </Button>
                    <Button onPress={() => { this.navigateToGame('2') }}>
                        <Image source={require('../../assets/images/img_btn2.png')} style={imgBtn} resizeMethod='resize' />
                    </Button>
                    <Button onPress={() => { this.navigateToGame('3') }}>
                        <Image source={require('../../assets/images/img_btn3.png')} style={imgBtn} resizeMethod='resize' />
                    </Button>

                </View>
                <View style={viewSettings}>
                    {/* <Button onPress={() => { this.navigateToGame('3') }}>
                        <Image source={require('../../assets/images/img_set1.png')} style={imgSet} resizeMethod='resize' />
                    </Button>
                    <Button onPress={() => { this.navigateToGame('3') }}>
                        <Image source={require('../../assets/images/img_set2.png')} style={imgSet} resizeMethod='resize' />
                    </Button>
                    <Button onPress={() => { this.navigateToGame('3') }}>
                        <Image source={require('../../assets/images/img_set3.png')} style={imgSet} resizeMethod='resize' />
                    </Button> */}
                 
                </View>
            </ImageBackground>


        )
    }
}

export default HomeScreen