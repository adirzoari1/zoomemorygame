import { StyleSheet, Dimensions, PixelRatio } from 'react-native'
import { calcSize } from '../../utils/Consts'
import colors from '../../utils/Colors'
import { DIMENSIONS } from '../../utils/Consts';
import Fonts from '../../utils/Fonts';


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    viewLogo:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: calcSize(150),
        marginBottom: calcSize(60)
    },
    imgLogo:{
        width: calcSize(202),
        height: calcSize(103),
    },
    textLogo:{
        fontSize:calcSize(60),
        color: colors.white,
        fontFamily: Fonts.paci_regular,

    },
    imgBtn:{
        width: calcSize(552),
        height: calcSize(181)
    },
    viewSettings:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        position: 'absolute',
        bottom: calcSize(40),
        padding: calcSize(30)
    },
    imgSet:{
        width: calcSize(145),
        height: calcSize(145),
        margin: calcSize(20)
    }

})

export default Styles
