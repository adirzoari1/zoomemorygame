
const colors = {
    transparent:'rgba(0,0,0,0)',
    white: '#fff',
    grey: '#fefaec',

    yellow: '#fac53b',
    yellow_a: '#fee13d',
    orange: '#ff8e0c',
    orange_a: '#faac27',
    blue: '#03051e'
}

export default colors