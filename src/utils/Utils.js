import Sound from 'react-native-sound'

let music = null;
export const playSound = (sound) => {
	if (music != null) { music.release() }
	music = new Sound(sound, Sound.MAIN_BUNDLE, (error) => {
		if (error) {
			console.log('failed to load the sound', error);
			return;
		}
		// music.setVolume(0.8)
		// loaded successfully, play			
		music.play((success) => {
			console.log(success)
			if (success) {
				console.log('successfully finished playing');
				music.release();
			} else {
				console.log('playback failed due to audio decoding errors');
			}
		});
	});

}