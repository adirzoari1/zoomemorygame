import { Dimensions } from 'react-native'
import DeviceInfo from 'react-native-device-info'

const edegesModels = [
    'SM-G935F',
    'SM-G925T',
    'SM-G925S',
    'SM-G925P',
    'SM-G925A',
    'SM-G950W',
    'SM-G9500',
    'SM-G9508',
    'SM-G950F',
    'E6653'

]
const DEVICE_MODEL = DeviceInfo.getModel()
const { height, width } = Dimensions.get('window')
const IPHONE_7_RESOLUTION = Math.sqrt(375 * 375 + 672 *672)
const CURRENT_RESOLUTION = Math.sqrt(height * height + width * width)
export const DIMENSIONS = { height, width }

export const RESOLUTIONS_PROPORTION = CURRENT_RESOLUTION / IPHONE_7_RESOLUTION
export const EDGE_DEVICE = height/width > 1.9 || DEVICE_MODEL.includes('S7') || edegesModels.indexOf(DEVICE_MODEL)>-1
export const IPHONE_X = DeviceInfo.getModel()==='iPhone X'


const DESIGN_WIDTH = 750
const PIXEL_RATIO = DESIGN_WIDTH / width


export const calcSize = size => size / PIXEL_RATIO



